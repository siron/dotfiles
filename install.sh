#!/bin/bash

script_dirpath=$(dirname $(readlink -f $0))
script_dirname=$(basename $script_dirpath)

ignored_files="\.git|readme|install"

if [[ "$PWD" != "$script_dirpath" ]]; then
    echo "This script must be ran within its dir"
    exit 1
fi

for f in *; do
    if [[ ! $f =~ $ignored_files ]]; then
        if [[ -e $HOME/.$f ]]; then
            echo "$HOME/.$f already exist"
            continue
        fi
        ln -s $script_dirname/$f $HOME/.$f
    fi
done

