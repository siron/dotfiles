# skip sourcing global rcs in /etc
unsetopt global_rcs

# detect proper terminfo and try set 256color term
#
# xterm
if [[ "$TERM" = "xterm" ]]; then
    if [[ -z "$COLORTERM" ]]; then
        if [[ -z "$XTERM_VERSION" ]]; then
            echo "Warning: Terminal wrongly calling itself 'xterm'."
        else
            case "$XTERM_VERSION" in
                "XTerm(256)") TERM="xterm-256color" ;;
                "XTerm(88)") TERM="xterm-88color" ;;
                "XTerm") ;;
                *)
                    echo "Warning: Unrecognized XTERM_VERSION: $XTERM_VERSION"
                    ;;
            esac
        fi
    else
        case "$COLORTERM" in
            gnome-terminal)
                # Those crafty Gnome folks require you to check COLORTERM,
                # but don't allow you to just *favor* the setting over TERM.
                # Instead you need to compare it and perform some guesses
                # based upon the value. This is, perhaps, too simplistic.
                #TERM="gnome-256color"

                # ! switched to xterm-256color because some console apps such
                # as ssh and mutt showed some quirks with gnome-256color
                TERM="xterm-256color"
                ;;
            # xfce4-terminal
            terminal|xfce4-terminal)
                TERM="xterm-256color"
                ;;
            # kde konsole
            konsole)
                TERM="konsole-256color"
                ;;
            *)
                echo "Warning: Unrecognized COLORTERM: $COLORTERM"
                ;;
        esac
    fi

    # if KDE konsole doesn't set $COLORTERM
    if [[ -n $KONSOLE_DBUS_SESSION ]]; then
        TERM="konsole-256color"
    fi

# GNU/screen or tmux
elif [[ $TERM == "screen" ]]; then
    TERM="screen-256color"

# putty/kitty
elif [[ $TERM == "putty" ]]; then
    TERM="putty-256color"
fi

# fall back
SCREEN_COLORS="$(tput colors)"
if [[ -z "$SCREEN_COLORS" ]]; then
    case "$TERM" in
        screen-*color-bce)
            echo "Unknown terminal $TERM. Falling back to 'screen-bce'."
            TERM=screen-bce
            ;;
        *-88color)
            echo "Unknown terminal $TERM. Falling back to 'xterm-88color'."
            TERM=xterm-88color
            ;;
        *-256color)
            echo "Unknown terminal $TERM. Falling back to 'xterm-256color'."
            TERM=xterm-256color
            ;;
    esac
    SCREEN_COLORS=$(tput colors)
fi
if [[ -z "$SCREEN_COLORS" ]]; then
    case "$TERM" in
        gnome*|xterm*|konsole*|aterm|[Ee]term)
            echo "Unknown terminal $TERM. Falling back to 'xterm'."
            TERM=xterm
            ;;
        rxvt*)
            echo "Unknown terminal $TERM. Falling back to 'rxvt'."
            TERM=rxvt
            ;;
        screen*)
            echo "Unknown terminal $TERM. Falling back to 'screen'."
            TERM=screen
            ;;
    esac
    SCREEN_COLORS=$(tput colors)
fi

export EDITOR="vim"
export GREP_OPTIONS='--color=auto'
export LESS='-irM -x4'

# export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
# export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
# export LESS_TERMCAP_me=$'\E[0m'           # end mode
# export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
# export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
# export LESS_TERMCAP_ue=$'\E[0m'           # end underline
# export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

# quilt
export QUILT_PATCHES="debian/patches"
export QUILT_PATCH_OPTS="--reject-format=unified"
export QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto"
export QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
export QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33:diff_ctx=35:diff_cctx=33"

# virtualenv
export PY_VIRTENV=$HOME/virtenv

# wine
export WINEDLLOVERRIDES="winemenubuilder.exe=d"
export WINEDEBUG="-all"

