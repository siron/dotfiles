# Description: List or kill all zombie processes without any mercy
#
# Usage: zombie list|kill

zombie() {
    case "$1" in
        list) ps -u $USER -o state,pid,comm | awk '$1 == "Z" { print }' ;;
        kill) ps -u $USER -o state,pid,comm | awk '$1 == "Z" { print $2 }' | xargs kill -9 ;;
        *) echo "Usage: zombie list|kill" >&2; return 1 ;;
    esac
}
