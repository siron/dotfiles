# Description: Control multiple services at once so you can do -
# for example: multi-service start nginx php5-fpm mysql
# to start LEMP stack with a single command

multi-service() {
    if [[ $# -le 1 ]]; then
        echo "Usage: multi-service start|stop|restart|status <service>[ <service>...]" >&2
        return 1
    fi

    local cmd="$1"
    case $cmd in
        start|stop|restart|status) shift ;;
        *)
            echo "Error: Bad command: $cmd" >&2
            return 1
            ;;
    esac

    local svc
    for svc in "$@"; {
        if [[ ! -e /etc/init/${svc}.conf && ! -e /etc/init.d/$svc ]]; then
            echo "Error: Unknown service: $svc" >&2
            continue
        fi

        sudo echo $fg_bold[white]$svc:$reset_color; service $svc $cmd
    }
}
