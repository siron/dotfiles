# Description: Download file with parallel connections using pget in lftp
# Usage: pget [-b <ip to bind to>] [-l <rate/sec in kB>] [-t <timeout in sec>] URL

pget() {
    # check if we have the external command required
    if ! hash lftp &>/dev/null; then
        echo "E: lftp command is not installed" >&2
        return 1
    fi

    local flag bind_ip limit_rate timeout
    while getopts ":b:l:t:" flag; do
        case $flag in
            b) bind_ip="$OPTARG" ;;
            l) limit_rate="$OPTARG" ;;
            t) timeout="$OPTARG" ;;
            \?) echo "E: Unknown option: -$OPTARG" >&2; return 1 ;;
        esac
    done

    if (( OPTIND != $# )); then
        echo "E: Bad argument or file URL not given" >&2
        return 1
    fi

    shift $((OPTIND - 1))

    # convert limit_rate to bytes
    (( limit_rate >= 1 )) && limit_rate=$((limit_rate * 1024)) || limit_rate=0

    lftp -c "set net:socket-bind-ipv4 ${bind_ip:-0.0.0.0}; set net:limit-total-rate ${limit_rate:-0}:0; set net:timeout ${timeout:-10}; pget -n 4 -c $1"
}
