# Description: Highlight all occurrences of texts passed in arguments
# Usage: command | hilight foo bar

hilight() {
    [[ $# -eq 0 ]] || egrep -i "^|${(j:|:)@}"
}
