# Description: Make "reset" also clear tmux buffer history when running in tmux

reset() {
    if (( $# >= 1 )); then
        command reset "$@"
    else
        command reset
    fi

    [[ "$TERM" =~ screen ]] && tmux clear-history
}
