# Description: List or remove all files that wine creates in user dir

list-wine-garbage() {
    emulate -L zsh
    unsetopt nomatch

    ls -dl \
        ~/.wine* \
        ~/.config/menus/applications-merged/wine* \
        ~/.local/share/applications/wine-extension* \
        ~/.local/share/desktop-directories/wine* \
        ~/.local/share/icons/hicolor/**/*wine* \
        ~/.local/share/mime/application/x-wine-extension* \
        ~/.local/share/mime/packages/x-wine* 2>/dev/null
}

rm-wine-garbage() {
    emulate -L zsh
    unsetopt nomatch
    
    rm -rf \
        ~/.wine \
        ~/.config/menus/applications-merged/wine* \
        ~/.local/share/applications/wine-extension* \
        ~/.local/share/desktop-directories/wine* \
        ~/.local/share/icons/hicolor/**/*wine* \
        ~/.local/share/mime/application/x-wine-extension* \
        ~/.local/share/mime/packages/x-wine*
}
