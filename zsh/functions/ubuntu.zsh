# Description: List all installed packages on Ubuntu family OS
# Notes: use -c to list only custom packages aka packages that were installed by yourself


_print_all_installed_packages() {
    dpkg-query -W -f='${binary:Package;-50} ${Version;-30}\n'
}

_prompt_distro_name() {
    echo "This is one time question to determine the distro name that
you're using from *buntu desktop distro family which will be used to download
the manifest file for the distro iso image from Canonical server.
Your answer will be written to $fg_bold[white]${XDG_CACHE_HOME:-$HOME/.cache}/listinstalledpkgs$reset_color
If you need to change your answer here in future, just delete the file to see
this message again.

Please select the *buntu desktop distro that this computer is running:
1) Ubuntu
2) Kubuntu
3) Lubuntu
4) Xubuntu
5) Ubuntu-Gnome"

    local choice
    while true; do
        read choice\?"Answer [1-5]: "
        if [[ -n $choice ]] && (( $choice >= 1 && $choice <= 5 )); then
            break
        fi
    done

    # find out the arch
    local arch
    [[ $(arch) == "x86_64" ]] && arch=amd64 || arch=i386

    local -a distroname
    local manifestfile
    distroname=(ubuntu kubuntu lubuntu xubuntu ubuntu-gnome)
    manifestfile="${distroname[choice]}-${DISTRIB_RELEASE}-desktop-${arch}.manifest"

    echo $manifestfile > ${XDG_CACHE_HOME:-$HOME/.cache}/listinstalledpkgs
}

_download_iso_manifest() {
    local cachedir=${XDG_CACHE_HOME:-$HOME/.cache}
    local manifestfile=$(< $cachedir/listinstalledpkgs)

    local url
    case ${manifestfile%%-*} in
              ubuntu) url="http://releases.ubuntu.com/${DISTRIB_CODENAME}" ;;
             kubuntu) url="http://cdimage.ubuntu.com/kubuntu/releases/${DISTRIB_CODENAME}/release" ;;
             lubuntu) url="http://cdimage.ubuntu.com/lubuntu/releases/${DISTRIB_CODENAME}/release" ;;
             xubuntu) url="http://cdimage.ubuntu.com/xubuntu/releases/${DISTRIB_CODENAME}/release" ;;
        ubuntu-gnome) url="http://cdimage.ubuntu.com/ubuntu-gnome/releases/${DISTRIB_CODENAME}/release" ;;
    esac

    echo "Downloading distro iso manifest..."
    if hash wget &>/dev/null; then
        wget -q -O $cachedir/$manifestfile "$url/$manifestfile"
    elif hash curl &>/dev/null; then
        curl -s -o $cachedir/$manifestfile "$url/$manifestfile"
    else
        echo "Aborting. Neither wget nor curl command is available" >&2
        exit 1
    fi
}

list-installed-packages() {
    if [[ $# -eq 0 ]]; then
        _print_all_installed_packages
        return
    elif [[ "$1" != "-c" ]]; then
        return
    else
        local arg showcustom=false nohide=false
        for arg; do
            case $arg in
                -c) showcustom=true ;;
                -a) nohide=true ;;
            esac
        done
    fi

    # require -c option to pass through this
    if ! $showcustom; then
        return
    fi

    # source lsb-release
    if [[ -f /etc/lsb-release ]]; then
        source /etc/lsb-release
    else
        echo "Failed to obtain OS release info" >&2
        return 1
    fi

    # are we running *buntu OS?
    if [[ -z $DISTRIB_ID  || ${DISTRIB_ID:l} != "ubuntu" ]]; then
        echo "Unsupported Linux distro" >&2
        return 1
    fi

    # get user cache dir
    local cachedir=${XDG_CACHE_HOME:-$HOME/.cache}

    # check if user has already run one-off setup
    if [[ ! -f $cachedir/listinstalledpkgs ]]; then
        _prompt_distro_name
    fi

    # read saved iso manifest filename
    local manifestfile=$(< $cachedir/listinstalledpkgs)
    if [[ -n $manifestfile ]]; then
        if [[ "${${(s:-:)manifestfile}[2]}" != "$DISTRIB_RELEASE" ]]; then
            _prompt_distro_name
            manifestfile=$(< $cachedir/listinstalledpkgs)
        fi
    else
        _prompt_distro_name
    fi

    # is the *buntu iso manifest already downloaded?
    if [[ ! -f $cachedir/$manifestfile ]]; then
        _download_iso_manifest
        if [[ $? -ne 0 ]]; then
            echo 'Download failed!' >&2
            return 1
        else
            echo "Download OK. Please run this command again."
            return 0
        fi
    fi

    # generate list of all installed packages
    _print_all_installed_packages > $cachedir/allpkgs

    # generate list of all packages manually installed by users
    comm -23 \
        <( dpkg --get-selections | awk '$2 ~ /^(install|hold)/ { print $1 }' | sort ) \
        <( awk '{ print $1 }' $cachedir/$manifestfile | sort ) > $cachedir/custompkgs

    # join lines from both lists if they contain common package name
    if $nohide; then
        awk 'FNR==NR {arr[$0];next} $1 in arr' $cachedir/custompkgs $cachedir/allpkgs
    else
        awk 'FNR==NR {arr[$0];next} $1 in arr' $cachedir/custompkgs $cachedir/allpkgs | egrep -v '^(lib|kernel)'
    fi

}
