# Description: quick command to manage repo source files in /etc/apt/sources.list.d
# Usage: apt-manage-repo enable|disable|remove|show|list [-b] [-s] <source.list>[ <source.list>...]

apt-manage-repo() {
    # path to sources.list.d
    local SOURCESLISTDIR="/etc/apt/sources.list.d"

    # command operation
    local action
    if [[ $# -eq 0 ]]; then
        # default action to "list" if no arg given
        action="list"
    else
        case "$1" in
            list)
                if [[ $# -ge 2 ]]; then
                    echo "Error: This command doesn't take arguments"
                    return 1
                fi
                action="list"
                ;;
            enable|disable|remove|show|update)
                action="$1"
                shift
                ;;
            *)
                echo "Error: Invalid operation: \"$1\"" >&2
                return 1
                ;;
        esac
    fi

    if [[ $action != "list" ]]; then
        local arg repobin=false reposrc=false
        local -aU sourceslist
        for arg; do
            case $arg in
                -b) repobin=true ;;
                -s) reposrc=true ;;
                *)
                    if [[ -z "$arg" ]]; then
                        continue
                    elif [[ ! -f "$SOURCESLISTDIR/$arg:t" ]]; then
                        echo "Error: $arg doesn't exist in $SOURCESLISTDIR" >&2
                        return 1
                    else
                        # collect all repo sources filename
                        sourceslist+=("$arg:t")
                    fi
                    ;;
            esac
        done

        # we don't want any duplicate source files
        #sourceslist=(${(u)sourceslist})

        # nothing to be done if $sourceslist is empty
        if [[ $#sourceslist -eq 0 ]]; then
            echo "Error: No repo source files to process" >&2
            return 1
        fi
    fi

    # do the magic
    case "$action" in
        enable)
                local sf
                if $repobin; then
                    # uncomment only deb
                    for sf in $sourceslist; sudo sed -i -r 's/^#\s*deb /deb /' $SOURCESLISTDIR/$sf
                elif $reposrc; then
                    # uncomment only deb-src
                    for sf in $sourceslist; sudo sed -i -r 's/^#\s*deb-src /deb-src /' $SOURCESLISTDIR/$sf
                else
                    # uncomment both deb and deb-src
                    for sf in $sourceslist; sudo sed -i -r 's/^#\s*deb/deb/' $SOURCESLISTDIR/$sf
                fi
                ;;
        disable)
                local sf
                if $repobin; then
                    # comment out only deb
                    for sf in $sourceslist; sudo sed -i -r 's/^deb /#deb /' $SOURCESLISTDIR/$sf
                elif $reposrc; then
                    # comment out only deb-src
                    for sf in $sourceslist; sudo sed -i -r 's/^deb-src /#deb-src /' $SOURCESLISTDIR/$sf
                else
                    # comment out both deb and deb-src
                    for sf in $sourceslist; sudo sed -i -r 's/^deb/#deb/' $SOURCESLISTDIR/$sf
                fi
                ;;
        list)
                local found state
                local deb_num src_num
                local sf line
                for sf ( $SOURCESLISTDIR/*.list(N) ); do
                    # use read and array because grep is slow
                    deb_num=0 src_num=0
                    while read -Ar line; do
                        [[ "${line[1]}" == "deb" ]] && deb_num+=1
                        [[ "${line[1]}" == "deb-src" ]] && src_num+=1
                    done < $sf

                    found=$((deb_num+src_num))
                    [[ $deb_num -eq 0 ]] && unset deb_num
                    [[ $src_num -eq 0 ]] && unset src_num
                    (( $found >= 1 )) && state="Enabled ${deb_num:+~deb} ${src_num:+~src}" || state="Disabled"
                    printf "%-50s : %s\n" $sf:t $state
                done
                ;;
        show)
                local sf
                for sf in $sourceslist; {
                    echo "$fg_bold[white]$sf$reset_color:"
                    grep --color=never "^deb" $SOURCESLISTDIR/$sf
                }
                ;;
        remove)
                local sf
                for sf in $sourceslist; sudo rm -i "$SOURCESLISTDIR/$sf"
                ;;
        update)
                local sf
                for sf in $sourceslist; {
                    sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/$sf" \
                        -o Dir::Etc::sourceparts="-" \
                        -o APT::Get::List-Cleanup="0"
                }
                ;;
    esac
}

