# Description: Make htop shows only processes from current user if launched without argument

htop() {
    hash htop &>/dev/null || return 1
    [[ $# -eq 0 ]] && command htop -u $USER || command htop "$@"
}
