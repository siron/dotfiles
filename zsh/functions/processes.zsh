
cpuhogger() {
    top -b -n 1 | tail -n +8 | sort -nrb -k9 | head -n 10 | \
        awk '
        BEGIN { printf "%5s %-10s %5s %5s %s\n", "PID","USER","%CPU","%MEM","COMMAND" }
        ! index($12, "top") { printf "%5i %-10s %5s %5s %s\n", $1,$2,$9,$10,$12 }'
}

memhogger() {
    top -b -n 1 | tail -n +8 | sort -nrb -k10 | head -n 10 | \
        awk '
        BEGIN { printf "%5s %-10s %5s %5s %s\n", "PID","USER","%CPU","%MEM","COMMAND" }
        ! index($12, "top") { printf "%5i %-10s %5s %5s %s\n", $1,$2,$9,$10,$12 }'
}