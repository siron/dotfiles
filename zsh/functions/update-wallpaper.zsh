# Description: Generate .xml list for background images on OS that use gnome3
#
# Usage: Drop all your background images into ~/.local/share/backgrounds then run
# this function


update-wallpaper() {
    emulate -L zsh
    setopt null_glob

    local bg_path="$HOME/.local/share/backgrounds"
    mkdir -p $bg_path

    local bg_xml="$HOME/.local/share/gnome-background-properties/customs.xml"
    mkdir -p $bg_xml:h

    # create list of background images
    local -a bg_list
    bg_list=($bg_path/*.{jpg,png})

    # if there are no images to process do not generate xml list
    if (( $#bg_list == 0 )); then
        echo "No background images found in $bg_path" >&2
        return 1
    fi

    # bg_xml header
    cat > $bg_xml <<EOL
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd">
<wallpapers>
EOL

    # generate a wp entry for each images found
    local bg_file i=0
    for bg_file in $bg_list
    do
        cat >> "$bg_xml" <<EOL
    <wallpaper>
        <name>$bg_file:t:r</name>
        <filename>$bg_file</filename>
        <options>zoom</options>
        <pcolor>#000000</pcolor>
        <scolor>#000000</scolor>
        <shade_type>solid</shade_type>
    </wallpaper>
EOL
        ((i++))
    done

    # bg_xml footer
    cat >> $bg_xml <<EOL
</wallpapers>
EOL

    # anddd we're done
    if [[ -s $bg_xml ]]; then
        echo "Done ($i images were processed)"
    fi
}
