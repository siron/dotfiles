# Description: Periodically monitor and watch your public IP for change

watch-ip() {
    emulate -L zsh
    zmodload zsh/datetime

    local old_ip ip day old_day
    while true;
    do
        ip=$(curl -s ifaddress.appspot.com)
        if [[ -n $ip && $ip != $old_ip ]]
        then
            day=$(strftime %a $EPOCHSECONDS)
            [[ $day != $old_day ]] && echo "----- $day -----"

            echo "$(strftime %r $EPOCHSECONDS) -> New IP: $ip"
            old_ip=$ip old_day=$day
        fi
        sleep 60
    done
}
