# Description: If file path is passed as arg, cd into the base dir of the file
# otherwise cd into path in arg as normally

fcd() {
    if (( $# == 1 )); then
        [[ -f "$1" ]] && cd "${1:h}" || cd "$1"
    fi
}
