# Description: Easily jump around the file system by manually adding marks.
# Marks are stored as symbolic links in the directory $MARKPATH (default to $HOME/.marks)
#
# Code is based on original jump plugin in Oh My Zsh
#
# Usage:
# jump FOO: jump to a mark named FOO
# mark FOO: create a mark named FOO
# unmark FOO: delete a mark
# marks: lists all marks

export MARKPATH=$HOME/.marks

jump() {
    cd -P "$MARKPATH/$1" 2>/dev/null || echo "No such mark: $1"
}

mark() {
    local mark
    if (( $# == 0 )); then
        mark="${PWD:t}"
    else
        mark="$1"
    fi

    if [[ -h "$MARKPATH/$mark" ]]; then
        local target=$MARKPATH/$mark
        echo "Mark: $mark already exists (target: ${target:A})" >&2
        return 1
    else
        local confirm
        read confirm\?"Mark $PWD as ${mark}? [y/N] "
        if [[ ${(L)confirm:0:1} == "y" ]]; then
            # create symlink for the mark
            mkdir -p "$MARKPATH"
            ln -s "$PWD" "$MARKPATH/$mark"
        fi
    fi
}

unmark() {
    local mark
    if (( $# == 0 )); then
        mark="${PWD:t}"
    else
        mark="$1"
    fi

    if [[ -h "$MARKPATH/$mark" ]]; then
        rm -i "$MARKPATH/$mark"
    fi
}

marks() {
    local link markname markpath
    for link in $MARKPATH/*(@N); do
        markname="$fg_bold[white]${link:t}$reset_color"
        markpath="${link:A}"
        printf "%-30s -> %s\n" $markname $markpath
    done
}
