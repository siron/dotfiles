# Description: Look up for word definition on definr.com
# Usage: definr <word>

definr() {
    # check if we have the external command required
    if ! hash w3m &>/dev/null; then
        echo "E: w3m command is not installed" >&2
        return 1
    fi

    if (( $# == 0 )); then
        echo "definr <word>" >&2
        return 1
    fi

    w3m -dump -no-cookie "definr.com/$*"
}
