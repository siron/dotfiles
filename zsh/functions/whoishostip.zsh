# Description: Resolve hostname to ip then whois the ip

whoishostip() {
    emulate -L zsh
    setopt rematch_pcre

    # check if we have the external commands required
    if ! hash dig &>/dev/null; then
        echo "E: dig command is not installed" >&2
        return 1
    elif ! hash whois &>/dev/null; then
        echo "E: whois command is not installed" >&2
        return 1
    fi

    # can't work without argument
    if [[ $# -eq 0 || -z "$1" ]]; then
        echo "whoishostip <hostname>" >&2
        return 1
    fi

    local validate_ip_regex
    local ip result

    validate_ip_regex="^(?:\d{1,3}\.){3}\d{1,3}$" #this will also match 999.999.999.999 but...whatever
    ip=$(dig +short "$1" a 2>/dev/null | while read -r result; do
        if [[ $result =~ $validate_ip_regex ]]; then
            echo $result
            break
        fi
    done)

    if [[ -z "$ip" ]]; then
        echo "E: Unable to resolve hostname: $1" >&2
        return 1
    else
        whois -H "$ip"
    fi
}
