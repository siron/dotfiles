#compdef hello
 

#$words contains list of words from current command line
#
#$state is a variable you can set in a special _arguments format ->value. \
#For example “1: :->test” means that if completion happens for the first argument \
#then the $state variable will have value “test”.


_hello() {
    local curcontext="$curcontext" state line
    typeset -A opt_args

    _arguments \
        '1: :->country'\
        '*: :->city'

    case $state in
        country)
            _arguments '1:Countries:(France Germany Italy)'
            ;;
        *)
            case $words[2] in
                France)
                    compadd "$@" Paris Lyon Marseille
                    ;;
                Germany)
                    compadd "$@" Berlin Munich Dresden
                    ;;
                Italy)
                    compadd "$@" Rome Napoli Palermo
                    ;;
                *)
                    _files
                    ;;
            esac
            ;;
    esac
}
 
_hello "$@"