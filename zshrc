# ------------------------------
# MODULES
# ------------------------------
#zmodload zsh/datetime


# ------------------------------
# SHELL OPT
# ------------------------------
setopt interactive_comments
#setopt rm_star_silent
#setopt print_exit_value
unsetopt beep
unsetopt hup
unsetopt flow_control
unsetopt bg_nice

# expansion and globing
setopt extended_glob
setopt glob_dots
#setopt null_glob
setopt magic_equal_subst
setopt mark_dirs
#setopt rematch_pcre
setopt brace_ccl
#unsetopt nomatch

# dir stack
setopt auto_pushd
setopt pushd_ignore_dups
DIRSTACKSIZE=10

# enable correction suggestion
setopt correct
CORRECT_IGNORE='_*'

# history
setopt append_history
setopt inc_append_history
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_find_no_dups

HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
HISTIGNORE=(
    clear exit echo exit reset
    clearclip dumpclip
)

# ------------------------------
# Completion
# ------------------------------
# load custom completion scripts
fpath+=($HOME/.zsh/completions)

setopt listrowsfirst
setopt glob_complete
setopt complete_in_word
#setopt always_to_end

autoload -Uz compinit && compinit -i -d "$HOME/.zcompdump-$ZSH_VERSION"

#zstyle :compinstall filename "$HOME/.zshrc"
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path $HOME/.zsh/cache/

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format '%BCompleting %d%b'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
#zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' use-compctl false
#zstyle ':completion:*' verbose true
zstyle ':completion:*' squeeze-slashes true

zstyle ':completion:*:functions' ignored-patterns '_*' '+*'
zstyle ':completion:*:cd:*' ignore-parents parent pwd

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always

zstyle ':completion:*:sudo:*' command-path /usr/local/sbin \
                                           /usr/local/bin  \
                                           /usr/sbin       \
                                           /usr/bin        \
                                           /sbin           \
                                           /bin            \
                                           /usr/X11R6/bin

# don't complete uninteresting users
() {
    local boring_users
    if [[ ! -e /etc/redhat-release ]]; then
        # debian variants and probably others
        boring_users=($(awk -F: '($3>0 && $3<1000) || $3 == 65534 { print $1 }' /etc/passwd))
    else
        # on rhel/centos variants
        boring_users=($(awk -F: '($3>0 && $3<500) || $3 == 65534 { print $1 }' /etc/passwd))
    fi
    zstyle ':completion:*:*:*:users' ignored-patterns $boring_users
}

# complete interesting hosts
() {
    # ssh
    local -a ssh_hosts
    [[ -r $HOME/.ssh/config ]] && \
        ssh_hosts=($(awk '$1 == "Host" && $2 ~ /^[a-zA-Z0-9-_.]+$/ { print $2 }' $HOME/.ssh/config))

    # lxc
    local -a lxc_containers lxc_hosts
    # since ubuntu 14.04 /var/lib/lxc is not world readable anymore
    # so this won't work without root priv
    #if [[ -r /var/lib/lxc/ && -r /etc/default/lxc-net ]]; then
    #    local lxc_domain=$(sed -rn 's/^LXC_DOMAIN="(.+)"/\1/p' /etc/default/lxc-net)
    #    if [[ -n $lxc_domain ]]; then

    #        lxc_containers=(/var/lib/lxc/*(N/:t))
    #        lxc_hosts=(${^lxc_containers}.$lxc_domain)
    #    fi
    #fi

    # workaround - use lxc hostname in dnsmasq lxc lease
    if [[ -r /etc/default/lxc-net ]]; then
        local lxc_domain=$(sed -rn 's/^LXC_DOMAIN="(.+)"/\1/p' /etc/default/lxc-net)
        if [[ -n $lxc_domain ]]; then
            lxc_hosts=($(awk '{ printf "%s.lxc\n", $4 }' /var/lib/misc/dnsmasq.lxcbr?.leases))
        fi
    fi

    # /etc/hosts
    local -a etc_hosts
    if [[ -r /etc/hosts ]]; then
        etc_hosts=($(awk '(!/^(#|$)/ && !/ip6/) { print $2 }' /etc/hosts))
    fi

    zstyle ':completion:*:ssh:*:hosts' hosts $ssh_hosts $lxc_hosts
    zstyle ':completion:*:ping:*:hosts' hosts $etc_hosts $lxc_hosts
}


# ------------------------------
# Prompt
# ------------------------------
setopt prompt_subst
unsetopt prompt_cr

autoload -Uz promptinit && promptinit
autoload -Uz colors && colors

# git prompt
autoload -Uz vcs_info
zstyle ':vcs_info:*' stagedstr '!'
zstyle ':vcs_info:*' unstagedstr '*'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' actionformats '%F{magenta}%b%F{white}|%a%f '
zstyle ':vcs_info:*' formats '%F{magenta}%s:%b%m %F{white}%c%u %f'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked git-status git-stash
zstyle ':vcs_info:*' enable git

+vi-git-untracked() {
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        git status --porcelain | grep -F '??' &> /dev/null ; then
        hook_com[unstaged]+='??'
    fi
}

# show remote ref name and number of commits ahead-of or behind
+vi-git-status() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    if [[ -n ${remote} ]] ; then
        ahead=$(git rev-list --count ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null)
        (( $ahead )) && gitstatus+=("+$ahead")

        behind=$(git rev-list --count HEAD..${hook_com[branch]}@{upstream} 2>/dev/null)
        (( $behind )) && gitstatus+=("-$behind")

        hook_com[misc]+=" [${remote} ${(j:/:)gitstatus}]"
    fi
}

# show count of stashed changes
+vi-git-stash() {
    local -a stashes

    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        stashes=$(git stash list 2>/dev/null | wc -l)
        hook_com[misc]+=" ($stashes stashed)"
    fi
}

# where are we..is this real env?
if [[ -z "$env_info" ]]; then
    # which container..
    # is it lxc?
    if [[ -r /proc/1/cgroup ]]; then
        grep -F '/lxc/' /proc/1/cgroup &>/dev/null
        [[ $status -eq 0 ]] && env_info+="(lxc)"
    fi
    # or chroot?
    if [[ -r /etc/debian_chroot ]]; then
        env_info+="(chroot:$(< /etc/debian_chroot))"
    fi

    # and are we in ssh session?
    [[ -n $SSH_CLIENT ]] && env_info+="(ssh)"

    # add a padding to right
    [[ -n $env_info ]] && env_info+=" "

    # finally set the var readonly
    typeset -r env_info
fi

if [[ -x /usr/bin/tput ]] && tput setaf 1 >&/dev/null; then
    #(lxc)(chroot:saucy)(ssh) user@hostname ~/.dotfiles git:master [origin/master ]
    PROMPT='$prompt_newline%B${env_info}${virtenv_info_msg}%F{green}%n@%m %F{blue}%~%f ${vcs_info_msg_0_}%b$prompt_newline%# '
    #RPROMPT='%F{245}%*%f'
else
    PROMPT='$prompt_newline${env_info}${virtenv_info_msg}%n@%m %~ ${vcs_info_msg_0_}$prompt_newline%# '
    #RPROMPT='%*'
fi

# set terminal title
_title() {
    case $TERM in
        screen*)
            # set screen hardstatus, usually truncated at 20 chars
            print -Pn "\ek$1:q\e\\"
            ;;
        xterm*|gnome*|rxvt*|ansi)
            # set window name
            print -Pn "\e]2;$2:q\a"
            # set icon (=tab) name (will override window name on broken terminal)
            print -Pn "\e]1;$1:q\a"
            ;;
    esac
}

_py_virtenv_info() {
    if [[ -r $PY_VIRTENV/bin ]]; then
        if [[ $PWD == $PY_VIRTENV* ]]; then
            if [[ -z $virtenv_info_msg ]]; then
                virtenv_info_msg="<virtenv> "
                path=($PY_VIRTENV/bin $path)
            fi
        else
            if [[ -n $virtenv_info_msg ]]; then
                unset virtenv_info_msg
                path=(${path#$PY_VIRTENV/bin})
            fi
        fi
    fi
}

_hook_precmd() {
    _title "%15<..<%~%<<" "zsh @ %~"
    vcs_info
}

_hook_preexec() {
    emulate -L zsh
    setopt extended_glob

    # cmd name only, or if this is sudo, the next cmd
    local cmd=${1[(wr)^(*=*|sudo|-*)]}
    [[ -n $cmd ]] && _title "$cmd" "$cmd"
}

_hook_zshaddhistory() {
    emulate -L zsh
    setopt extended_glob

    local cmd=${${1%%$'\n'}[(wr)^(*=*|sudo|-*)]}
    print -- $cmd
    if [[ -z "$cmd" ]]; then
        return 0
    elif whence "$cmd" &>/dev/null; then
        if [[ ${HISTIGNORE[@]} =~ "\b${cmd}\b" ]]; then
            #fc -p ~/.zsh_history_ignore
            #print -sr -- ${1%%$'\n'}
            return 1
        fi
        return 0
    fi

    return 1
}

_hook_cwd() {
    _py_virtenv_info
}

autoload -U add-zsh-hook
add-zsh-hook precmd  _hook_precmd
#add-zsh-hook preexec _hook_preexec
#add-zsh-hook zshaddhistory _hook_zshaddhistory
add-zsh-hook chpwd _hook_cwd


# ------------------------------
# Key bindings
# ------------------------------
# use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -A key

key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key accordingly
[[ -n "${key[Home]}"    ]] && bindkey "${key[Home]}"    beginning-of-line
[[ -n "${key[End]}"     ]] && bindkey "${key[End]}"     end-of-line
[[ -n "${key[Insert]}"  ]] && bindkey "${key[Insert]}"  overwrite-mode
[[ -n "${key[Delete]}"  ]] && bindkey "${key[Delete]}"  delete-char
[[ -n "${key[Up]}"      ]] && bindkey "${key[Up]}"      up-line-or-search
[[ -n "${key[Down]}"    ]] && bindkey "${key[Down]}"    down-line-or-search
[[ -n "${key[Left]}"    ]] && bindkey "${key[Left]}"    backward-char
[[ -n "${key[Right]}"   ]] && bindkey "${key[Right]}"   forward-char

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    _zle-line-init() {
        emulate -L zsh
        printf '%s' "${terminfo[smkx]}"
    }
    _zle-line-finish() {
        emulate -L zsh
        printf '%s' "${terminfo[rmkx]}"
    }
    zle -N _zle-line-init
    zle -N _zle-line-finish
fi

# history search
[[ -n "${key[PageUp]}"   ]] && bindkey "${key[PageUp]}"   history-beginning-search-backward
[[ -n "${key[PageDown]}" ]] && bindkey "${key[PageDown]}" history-beginning-search-forward

# copy the word before the cursor
# key: alt+m
#bindkey "^[m" copy-prev-shell-word
bindkey "^[m" end-of-history

_rationalise-dot() {
    if [[ $LBUFFER == *.. ]]; then
        LBUFFER+=/..
    else
        LBUFFER+=.
    fi
}
zle -N _rationalise-dot
bindkey . _rationalise-dot

_sudo-accept-line() {
    emulate -L zsh
    setopt extended_glob

    if [[ -n "${BUFFER##[\ ]#}" ]]; then
        if [[ $BUFFER[1] == ' ' ]]; then
            BUFFER=" sudo ${BUFFER[(wr)^(sudo),-1]}"
        else
            BUFFER="sudo ${BUFFER[(wr)^(sudo),-1]}"
        fi

        zle end-of-line
        zle accept-line
    fi
}
zle -N _sudo-accept-line
bindkey "^O" _sudo-accept-line

_greedy-backward-kill-word() {
    emulate -L zsh
    setopt extended_glob

    if [[ $LBUFFER[-1] == ' ' ]]; then
        LBUFFER=${LBUFFER%%[\ ]#}
    fi

    if [[ $LBUFFER[-1] == \" ]]; then
        LBUFFER=${LBUFFER%\"*\"}
    elif [[ $LBUFFER[-1] == \' ]]; then
        LBUFFER=${LBUFFER%\'*\'}
    else
        LBUFFER=${LBUFFER%%[^\ ]#}
    fi
}
zle -N _greedy-backward-kill-word
bindkey '^W' _greedy-backward-kill-word


# ------------------------------
# Aliases
# ------------------------------
alias ls='ls --color=auto --group-directories-first'
alias la='ls -alhF'
alias lA='ls -AlhF'
alias ll='ls -lhF'
alias lf='ls -lhF *(^/)'
alias l='ls -1'
alias sudo='sudo '
alias -g L='| less'
alias -g G='| grep'
alias -g eG='| egrep'
alias -g fG='| fgrep'
alias -g NN='>&! /dev/null'
alias -g ON='> /dev/null'
alias -g EO='2>&1'
alias -g EN='2> /dev/null'


# ------------------------------
# Etc.
# ------------------------------

# source other aliases
[[ -r $HOME/.zsh/aliases.zsh ]] && source $HOME/.zsh/aliases.zsh

# switching shell safely and efficiently?
# http://www.zsh.org/mla/workers/2001/msg02410.html
bash() {
    NO_SWITCH="yes" command bash "$@"
}

restart-shell() {
    exec $SHELL $SHELL_ARGS "$@"
}

# source other functions
if [[ -d $HOME/.zsh/functions ]]; then
    () {
        local func
        for func ($HOME/.zsh/functions/*.zsh(N)); source $func
    }
fi

# instead of passing non existing commands to cnf handler
# create a function to call it manually
if [[ -x /usr/lib/command-not-found ]] ; then
    function findcmdpkg() {
        /usr/lib/command-not-found --no-failure-msg -- $1
        return 0
    }
fi

# auto quote url when needed
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

zstyle -m ':urlglobber' url-local-schema '*' ||
    zstyle ':urlglobber' url-local-schema ftp file

zstyle -m ':urlglobber' url-other-schema '*' ||
    zstyle ':urlglobber' url-other-schema http https ftp magnet

# load zsh powerful built-in mv
autoload -Uz zmv

# calculator
autoload -Uz zcalc
autoload -Uz zmathfuncdef
