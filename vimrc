set nocompatible
set backspace=indent,eol,start
set showtabline=2
set mouse=a

syntax on
filetype plugin indent on

if &t_Co == 256
	colorscheme wombat256mod
endif

set ignorecase
set smartcase
set hlsearch
"set incsearch
"set showmatch
"set mat=5

set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

"set cursorline
set sidescroll=10
set nowrap
set number
set pastetoggle=<F4>
set list listchars=tab:»-,trail:.,precedes:<,extends:>

set foldmethod=manual
set foldlevel=1

if &diff
	set diffopt=filler,context:2 "
	set diffopt+=iwhite
endif

set showcmd
set showmode
set ruler
set rulerformat=%35(%=%{&filetype}\ %{&ff}\ %5.l,%-3.c\ %3.p%%%)


" Functions

" trim trailing whitespaces
function _trimwhitespace(...)
    " if no arg, trim trailing
    if a:0 == 0
        %s/\v[\t ]+$//
    elseif a:0 == 1
        " if arg is h trim header
        if a:1 == "h"
            %s/\v^[\t ]+//
        " both header and trailing if arg is ht
        elseif a:1 == "ht"
            %s/\v^[\t ]+//
            %s/\v[\t ]+$//
        endif
    endif
endfunction

" fold all comments
function _foldcomments()
	set foldmethod=expr
	set foldexpr=getline(v:lnum)=~'^#'
	set foldlevel=0
	set foldenable
:endfunction

" highlight trailing whitespaces
function _hlwhitespace(...)
    call clearmatches()
    :highlight whitespace ctermfg=yellow guifg=yellow

    if a:0 == 0
        let mt = matchadd('whitespace', '\v[\t ]+$')
    elseif a:0 == 1
        if a:1 == "h"
            let mh = matchadd('whitespace', '\v^[\t ]+')
        elseif a:1 == "t"
            let mt = matchadd('whitespace', '\v[\t ]+$')
        elseif a:1 == "ht"
            let mh = matchadd('whitespace', '\v^[\t ]+')
            let mt = matchadd('whitespace', '\v[\t ]+$')
        elseif a:1 == "c"
            call clearmatches()
        endif
    endif
endfunction


" Autocommands

" remove all trailing whitespaces on save
"autocmd FileWritePre * :call _trimwhitespace()
"autocmd FileAppendPre * :call _trimwhitespace()
"autocmd FilterWritePre * :call _trimwhitespace()
"autocmd BufWritePre * :call _trimwhitespace()

" set foldmethod to indent for python file
autocmd FileType python setlocal foldmethod=indent

autocmd BufReadPost * :DetectIndent

" Key mapping

" <Del> deletes line without copying into register
nnoremap <Del> "_dd
vnoremap <Del> "_d

" <Tab> indents selected lines when tab key is pressed
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

" <F2> saves file
"nnoremap <F2> :w<CR>
"inoremap <F2> <C-o>:w<CR>

" <F3> calls remove trailing whitespace func
nnoremap <F3> :call _trimwhitespace()<CR>
inoremap <F3> <C-o>:call _trimwhitespace()<CR>

" <F5> executes opened file
nnoremap <F5> :w<CR>:!%:p<CR>
inoremap <F5> <Esc>:w<CR>:!%:p<CR>

" <F9> toggles folding
nnoremap <F9> za
inoremap <F9> <C-o>za
onoremap <F9> <C-c>za
vnoremap <F9> zf

" <Ctrl+e+e> forces quit
nnoremap <C-e>e :q!<CR>
inoremap <C-e>e <Esc>:q!<CR>
vnoremap <C-e>e <Esc>:q!<CR>

" <ScrollWheel> scrolls up/down
"nnoremap <ScrollWheelUp> <C-y>
"inoremap <ScrollWheelDown> <C-e>

" I don't have a need for record function
" so override its keybind "q" and rebind it to quit instead
nnoremap q :q<CR>
vnoremap q <Esc>:q<CR>

" Move current line up/down
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" Duplicate line with Ctrl+d
nnoremap <C-d> yyP
vnoremap <C-d> yP


" Syntax plugin specific settings

" bash
let g:sh_fold_enabled=1
let g:is_bash=1

" python
let python_highlight_all=1

